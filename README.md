created by SF on 12.02.2024
in this experiment we want to test if the eye-movement behaviour is different for express and reflexive saccades.
we performed two different tasks: 
1) visually guided saccade task to four cardinal directions (left,right,up,down)
2) stimulus containing random dot stereogram which moves in a constant velocity in one of four cardinal direction, the direction of the dots should be the opposite direction of the preferred direction of the recorded cell in order to induce a reflexive saccade towards the preffered direction of the cell. for example if the cell is tuned to lefward saccade, the motion of the dots should be to the right (this movement will induce a reflexive saccade to the left wich is the preffered direction of the cell)
when you load trialdata

